<?php

namespace Drupal\config_features\Controller;

use Drupal\Core\Entity\EntityStorageException;
use Drupal\Core\File\FileSystemInterface;
use Drupal\config\Controller\ConfigController;
use Drupal\Core\Archiver\ArchiveTar;
use Drupal\Core\Messenger\MessengerTrait;
use Drupal\Core\Serialization\Yaml;
use Drupal\Core\StreamWrapper\StreamWrapperManager;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\Core\Url;
use Drupal\datetime\Plugin\Field\FieldType\DateTimeItemInterface;
use Drupal\file\Entity\File;
use Drupal\file\FileInterface;
use Drupal\system\FileDownloadController;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use Drupal\Core\StreamWrapper\StreamWrapperManagerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Config\StorageCopyTrait;

/**
 * Returns responses for config_batch_export module routes.
 */
class ConfigBatchExportController extends ConfigController {

  use StringTranslationTrait;
  use MessengerTrait;
  use StorageCopyTrait;

  /**
   * How many elements of the array to process per one operation.
   */
  const BATCH_SIZE = 10;

  const LOCK_ID = 'config_feature_batch_export_download';

  const FILE_PREFIX = 'config-feature--';


  /**
   * The feature manager.
   *
   * @var \Drupal\config_features\ConfigFeaturesManager
   */
  protected $manager;

  /**
   * The route match.
   *
   * @var \Drupal\Core\Routing\RouteMatchInterface
   */
  protected $routeMatch;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    $instance = parent::create($container);
    $instance->manager = $container->get('config_features.manager');
    $instance->routeMatch = $container->get('current_route_match');
    return $instance;
  }

  /**
   * File system.
   *
   * @return \Drupal\Core\File\FileSystemInterface
   *   File system.
   */
  protected static function getFilesystem() {
    return \Drupal::service('file_system');
  }

  /**
   * Batch callback.
   */
  public static function callbackBatchJob($batch_data, &$context) {
    $sandbox = &$context['sandbox'];
    if (!isset($sandbox['started'])) {
      /** @var \Drupal\file\FileInterface $file */
      $file = \Drupal::entityTypeManager()->getStorage('file')->load($batch_data['file_id']);

      $sandbox['filename'] = $file->getFileUri();
      $sandbox['started'] = TRUE;
      $sandbox['offset'] = 0;
      $sandbox['total'] = count($batch_data['configs']);
      $sandbox['batch_size'] = static::BATCH_SIZE;
      $context['results']['file_id'] = $batch_data['file_id'];
      $context['results']['config_feature'] = $batch_data['config_feature'];
    }

    $configs = array_slice($batch_data['configs'], $sandbox['offset'], $sandbox['batch_size']);
    $collection_storage_cache = [];
    if (!empty($configs)) {
      // Create uncompressed archive.
      $archiver = new ArchiveTar($sandbox['filename']);

      // $config_factory = \Drupal::configFactory();
      /** @var \Drupal\config_features\ConfigFeaturesManager $manager */
      $manager = \Drupal::service('config_features.manager');
      $feature = $manager->getFeatureConfig($batch_data['config_feature']);
      // $feature_preview = $manager->singleExportPreview($feature);
      $export = $manager->getExportStorage($feature);
      /** @var \Drupal\Core\Config\StorageInterface $target_storage */
      $target_storage = \Drupal::service('config.storage');
      foreach ($configs as $config_definition) {
        if (is_array($config_definition)) {
          // Process collection config.
          $name = $config_definition['name'];
          $collection = $config_definition['collection'];
          $collection_storage = $collection_storage_cache[$collection] ?? ($collection_storage_cache[$collection] = $target_storage->createCollection($collection));
          $archiver->addString(str_replace('.', '/', $collection) . "/$name.yml", Yaml::encode($collection_storage->read($name)));
        }
        else {
          $name = $config_definition;
          $archiver->addString("$name.yml", Yaml::encode($export->read($name)));
        }
      }

      $context['finished'] = $sandbox['offset'] / $sandbox['total'];

      // Call destructor.
      unset($archiver);
    }
    else {
      $context['finished'] = 1;
    }

    $sandbox['offset'] += $sandbox['batch_size'];

    $context['message'] = t('Processed @current out of @total', [
      '@current' => $context['sandbox']['offset'],
      '@total' => $context['sandbox']['total'],
    ]);
  }

  /**
   * Batch finished callback.
   */
  public static function callbackBatchFinished($status, $results) {
    /** @var \Drupal\file\FileInterface $file */
    $file = \Drupal::entityTypeManager()->getStorage('file')->load($results['file_id']);
    $oldpath = $file->getFileUri();

    $file->setFilename(self::FILE_PREFIX . $results['config_feature'] . '.tar');
    $file->setFileUri('private://' . $file->getFilename());

    if (extension_loaded('zlib')) {
      $file->setFilename($file->getFilename() . '.gz');
      $file->setFileUri('private://' . $file->getFilename());

      $gzfp = gzopen($file->getFileUri(), 'wb');
      $tarfp = fopen($oldpath, 'rb');
      while (!feof($tarfp) && FALSE !== ($buff = fread($tarfp, 4096))) {
        gzwrite($gzfp, $buff);
      }

      gzclose($gzfp);
      fclose($tarfp);
    }
    else {
      $destination =  static::getFilesystem()->move($oldpath, 'private://', FileSystemInterface::EXISTS_REPLACE);

      @rename($destination, $file->getFileUri());
    }

    // Ensure the temp file is deleted.
    static::getFilesystem()->delete($oldpath);

    $file->save();

    $url = Url::fromRoute('config_features.export_download_file', [
      'config_feature' => $results['config_feature'],
      'file' => $file->id(),
    ]);

    \Drupal::messenger()->addMessage(t('Config feature export file created at @created can be downloaded here <a href="@url">here</a>', [
      '@url' => $url->toString(),
      '@created' => date(DateTimeItemInterface::DATE_STORAGE_FORMAT . ' H:i:s'),
    ]));

    static::unlock();

    return new RedirectResponse(Url::fromRoute('entity.config_feature.export', ['config_feature' => $results['config_feature']])->toString());
  }

  /**
   * Downloads a tarball of the site configuration.
   */
  public function downloadExport() {
    $feature = $this->getFeature();

    // First prepare preview storage
    $preview = $this->manager->singleExportPreview($feature);

    // Now get export storage, it may be empty or not.
    $export = $this->manager->getExportStorage($feature);


    self::replaceStorageContents($preview, $export);
    
    $redirect_url = Url::fromRoute('config_features.export_download', ['config_feature' => $feature->get('id')])->toString();
    if (!static::lock()) {
      $this->messenger()->addError($this->t("Can't lock the operation"));
      return new RedirectResponse($redirect_url);
    }

    $filepath = $this->fileSystem->tempnam('temporary://', hash('adler32', self::FILE_PREFIX . $feature->get('id')));

    // @todo: Use dependency injection for all \Drupal calls.
    // @todo: Use different temporary files with random names.
    // @todo: Control gurbage collection.
    $existing_files = \Drupal::entityTypeManager()->getStorage('file')->loadByProperties(['uri' => 'private://' . self::FILE_PREFIX . $feature->get('id') . '.tar.gz']);
    $existing_files += \Drupal::entityTypeManager()->getStorage('file')->loadByProperties(['uri' => 'private://' . self::FILE_PREFIX . $feature->get('id') . '.tar']);
    if (!empty($existing_files)) {
      /** @var \Drupal\file\FileInterface $file */
      foreach ($existing_files as $file) {
        try {
          $file->delete();
        }
        catch (EntityStorageException $e) {
          watchdog_exception('config_batch_export.routing', $e);
        }

        if (file_exists($file->getFileUri())) {
          $this->fileSystem->delete($file->getFileUri());
        }
      }
    }

    // Ensure the file is deleted.
    if (file_exists($filepath)) {
      $this->fileSystem->delete($filepath);
    }

    $file = File::create([
      'uri' => $filepath,
      'uid' => \Drupal::currentUser()->id(),
    ]);

    $file->setTemporary();
    try {
      $file->save();
    }
    catch (EntityStorageException $e) {
      watchdog_exception('config_batch_export.routing', $e);
    }

    // $feature_preview = $this->manager->singleExportPreview($feature);


    $batch_data = [
      'config_feature' => $feature->get('id'),
      'file_id' => $file->id(),
      'configs' => $export->listAll(),
    ];

    $batch = [
      'title' => $this->t('Generation of a configs archive'),
      'operations' => [
        [
          [static::class, 'callbackBatchJob'],
          [$batch_data],
        ],
      ],
      'finished' => [static::class, 'callbackBatchFinished'],
      'batch_redirect' => Url::fromRoute('entity.config_feature.export', [
        'config_feature' => $feature->get('id'),
      ])
    ];

    batch_set($batch);

    return batch_process($redirect_url);
  }

  /**
   * Tries to lock the operation.
   *
   * @param bool $force
   *
   * @return bool
   */
  public static function lock($force = FALSE) {
    if ($force) {
      static::unlock(TRUE);
    }

    return static::lockBackend()->acquire(static::LOCK_ID, 600);
  }

  /**
   * Unlocks the operation.
   *
   * @param boolean $force
   *   If true, will ensure the lock is removed.
   */
  public static function unlock($force = FALSE) {
    // @todo: In case if $force = TRUE ensure the lock is deleted.
    static::lockBackend()->release(static::LOCK_ID);
  }

  /**
   * Get lock backend.
   *
   * @return \Drupal\Core\Lock\LockBackendInterface
   */
  protected static function lockBackend() {
    return \Drupal::service('lock.persistent');
  }

  /**
   * Checks lock status.
   *
   * @return bool
   *   TRUE, if locked. FALSE - if unlocked.
   */
  public static function isLocked() {
    $is_unlocked = static::lockBackend()->lockMayBeAvailable(static::LOCK_ID);
    return !$is_unlocked;
  }

  /**
   * Downloads a file.
   *
   * @param \Drupal\file\FileInterface $file
   *   File object.
   *
   * @return \Symfony\Component\HttpFoundation\BinaryFileResponse
   *   Response.
   *
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  public function downloadExportFile(FileInterface $file) {
    if (static::isLocked()) {
      throw new AccessDeniedHttpException();
    }

    $request = new Request([
      'file' => basename($file->getFileUri()),
    ]);

    // Make the file old to delete it in file_cron.
    $file
      ->setChangedTime(1)
      ->save();

    $scheme = StreamWrapperManager::getScheme($file->getFileUri());
    $download_controller = new FileDownloadController(\Drupal::service('stream_wrapper_manager'));
    $response = $download_controller->download($request, $scheme);
    return $response;
  }

  /**
   * Get a feature from the route.
   *
   * @return \Drupal\Core\Config\ImmutableConfig
   *   The feature config.
   */
  protected function getFeature() {
    $feature = $this->manager->getFeatureConfig($this->routeMatch->getRawParameter('config_feature'));
    if ($feature === NULL) {
      throw new \UnexpectedValueException("Unknown feature");
    }
    return $feature;
  }

}
